

import React from 'react'

const API = 'https://drugtargetcommons.fimm.fi/api/data/bioactivity/?format=json';
const DEFAULT_QUERY = 'redux';
//const obj = JSON.parse(API); 


export default class FetchData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bioactivities: []

        };
    }
    componentDidMount() {
        fetch(API + DEFAULT_QUERY)
            .then(response => response.json())
            .then(data => this.setState({ bioactivities: data.bioactivities })).catch('Error');
    }
    render() {
        const { bioactivities, isLoading } = this.state;
        if (isLoading) {
            return <p>Loading...</p>
        }


        return (
            <div>
                <ul>
                    {bioactivities.map(bioactivity =>
                        <li key={bioactivity.gene_name}>
                            <a href={bioactivity.url}>Activity comment for "{bioactivity.gene_name}":"{bioactivity.activity_comment}" </a>
                        </li>)}
                </ul>
            </div >

        );
    }
}