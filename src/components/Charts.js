import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import Fetchdata from "./FetchData.js"
/*const options = {
    chart: {
        type: 'pie'
    },
    title: {
        text: 'Last 10 Superleauge title winners    '
    },
    series: [
        {
            data: [{
                y: 1,
                name: 'Panathinaikos',
                color: 'green'
            }, {
                y: 1,
                name: 'Aek',
                color: 'yellow'
            }, {
                y: 1,
                name: 'Paok',
                color: 'black'
            }, {
                y: 7,
                name: 'Olympiakos',
                color: 'red'
            }]
        }
    ]
};
*/
const options2 = {
    chart: {
        type: 'line'
    },
    title: {
        text: 'Bioactivity'
    },
    series: [
        {
            data: {< Fetchdata />}

        }
    ]
};
class Charts extends React.Component {

    render() {
        return (
            <div>
                <HighchartsReact highcharts={Highcharts} options={options2} />
                {/*<HighchartsReact highcharts={Highcharts} options={options2} />*/}
            </div>
        )
    }

}
export default Charts