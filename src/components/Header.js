import React from "react"
import Frame from 'react-frame-component';
import Charts from './Charts'
import { Link } from "react-router-dom";
import { Nav, Navbar, NavItem } from "react-bootstrap";
import Routes from "../Routes";
import { LinkContainer } from "react-router-bootstrap";

//When line 31 exists, Module not found: Can't resolve 'inspector' in '/Users/dimitrisliaropoulos/Desktop/firsttask/intelligenciafirst/src'
/* <button > Home </button>
<input type="button" value="Bulk import" />
<input type="button" value="Take a tour" />
<input type="button" value="Download" />
<input type="button" value="Reference" />
<input type="button" value="Help" />
<input type="button" value="Login" />
*/


class Header extends React.Component {

    render() {

        return (
            <div>
                <Frame className="toolbar">
                    <img src={require("./inteligencia.png")} alt="" />
                    <div className="App container">
                        <Navbar>
                            <Navbar.Header>
                                <Navbar.Brand>
                                    <Link to="/">Home</Link>
                                </Navbar.Brand>
                                <Navbar.Toggle />
                            </Navbar.Header>
                            <Navbar.Collapse>
                                <Nav pullRight>
                                    <LinkContainer to="/signup">
                                        <NavItem>Signup</NavItem>
                                    </LinkContainer>
                                    <LinkContainer to="/login">
                                        <NavItem>Login</NavItem>
                                    </LinkContainer>
                                </Nav>
                            </Navbar.Collapse>
                        </Navbar>
                        <Routes />

                    </div>
                </Frame >
                <div className="headr">
                    <h1>Drug Target Commons (DTC)</h1>
                    <p>
                        Drug Target Commons (DTC) is a crowd-sourcing platform to improve the consensus
                    and use of drug-target interactions. The end users can search, view and download
                    bioactivity data using various compound, target and publications identifiers.
                    Expert users may also submit suggestions to edit and upload new bioactivity data,
                    as well as participate in the assay annotation and data curation processes.
                            </p>
                    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for a drug.."></input>

                    <Frame className="frame">
                        <Charts />


                    </Frame>
                </div>
            </div >
        );
    }

}

export default Header;