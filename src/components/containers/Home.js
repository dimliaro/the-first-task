import React from "react";
import "./Home.css";

export default function Home() {
    return (
        <div className="Home">
            <div className="lander">
                <h1>DTC</h1>
                <p>Drug Target Commons</p>
            </div>
        </div>
    );
}